# =========================================== CI/CD Demo with Jenkins ===============================================================

# ----------------------- SETUP -------------------------------------------------------------------
# Will intall all the infrastructure required in AWS LS
infra:
	@echo "== Installing Infrastructure =="
	@./scripts/terraform-deploy.sh

# Will deploy artifactory and jenkins in a given instance/server
platform:
	@echo "== Installing Platform and Deploy Artifactory and Jenkins =="
	@./scripts/ansible-playbooks.sh

# Will remove all the resources deployed in AWS
destroy:
	@echo "== Destroy All Deployed Resources =="
	@./scripts/clean-up.sh
	@./scripts/terraform-destroy.sh

# Will install a server using vagrant in local VirtualBox
infra-vagrant:
	@echo "== Installing Infrastructure with Vagrant =="
	@./scripts/vagrant-deploy.sh

destroy-vagrant:
	@echo "== Destroy All Deployed Resources in Vagrant =="
	@./scripts/vagrant-destroy.sh

create-ssh-key:
	@echo "create key here"
	@ssh-keygen -t rsa -N "" -f mykey

clean-up:
	@echo "cleaning previous run if any"
	@./scripts/clean-up.sh

install: clean-up create-ssh-key infra platform

install-vagrant: clean-up create-ssh-key infra-vagrant platform
