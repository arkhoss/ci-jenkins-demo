# How to Install VVADAJA

## Prerequisites

1. UNIX/Linux Env or Microsoft WSL
2. Git
3. VirtualBox
4. Vagrant
5. Terraform
6. Ansible
7. Text Editor (Recommended VSC)

## What is going to Happen?

At the end of this demo, you will have an VM machine running a docker service with fully deployed Artifactory and Jenkins, both as docker containers, With the possibility to run any dockerized application.

## How to Deploy


Clone the repository ci-jenkins-demo

    ~$ git clone git@gitlab.com:arkhoss/ci-jenkins-demo.git && cd ci-jenkins-demo

Install

    ~$ make install-vagrant

## Check and Usage

Go to http://<InstanceIP>:<Port>/

Ports:

Application: 4200

Jenkins UI: 8080

Artifactory UI: 8081

## How to Destroy

     ~$ make destroy-vagrant
