resource "aws_lightsail_key_pair" "ci-jenkins-demo-key-pair" {
  name       = "MYKEYPAIR"
  public_key = file(var.PATH_TO_PUBLIC_KEY)
}

resource "aws_lightsail_instance" "ci-jenkins-demo" {
  name              = "ci-jenkins-demo"
  availability_zone = var.AVAILABILITY_ZONE
  blueprint_id      = var.BLUEPRINT_ID
  bundle_id         = var.BUNDLE_ID
  key_pair_name     = aws_lightsail_key_pair.ci-jenkins-demo-key-pair.name
  tags              = var.TAGS

  provisioner "local-exec" {
    command = "echo ${aws_lightsail_instance.ci-jenkins-demo.public_ip_address} >> ../server_ip"
  }
}
