
resource "aws_lightsail_instance" "ci-jenkins-demo" {
  name              = "ci-jenkins-demo"
  availability_zone = var.AVAILABILITY_ZONE
  blueprint_id      = var.BLUEPRINT_ID
  bundle_id         = var.BUNDLE_ID
  key_pair_name     = var.KEYNAME
  tags              = var.TAGS
}
