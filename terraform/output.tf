output "lightsail" {
  value = aws_lightsail_instance.ci-jenkins-demo.public_ip_address
}
