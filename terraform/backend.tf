terraform {
  backend "s3" {
    bucket = "dc-dev-terraform"
    key    = "terraform/state"
    region = "us-east-1"
 }
}
