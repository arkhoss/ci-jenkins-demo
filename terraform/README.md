# How to Install TAADAJA

## Prerequisites

1. UNIX/Linux Env or Microsoft WSL
2. Git
3. An AWS Account with AWS LightSail Permissions
4. AWS CLI Credentials
5. AWS CLI
6. Terraform
7. Ansible
8. Text Editor (Recommended VSC)

## What is going to Happen?

At the end of this demo, you will have an AWS LightSail instance running a docker service with fully deployed Artifactory and Jenkins, both as docker containers, With the possibility to run any dockerized application.

## How to Deploy

Clone the repository ci-jenkins-demo

    ~$ git clone git@gitlab.com:arkhoss/ci-jenkins-demo.git && cd ci-jenkins-demo

Install

    ~$ make install

##  Check and Usage


Go to http://<InstanceIP>:<Port>/

Ports:

Application: 4200

Jenkins UI: 8080

Artifactory UI: 8081

## How to Destroy

    ~$ make destroy
