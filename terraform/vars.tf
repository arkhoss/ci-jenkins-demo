variable "AWS_REGION" {
  default = "us-east-1"
}

variable "AVAILABILITY_ZONE" {
  default = "us-east-1c"
}

variable "BLUEPRINT_ID" {
  default = "centos_7_1901_01"
}

variable "BUNDLE_ID" {
  default = "medium_2_0"
}

variable "PATH_TO_PRIVATE_KEY" {
  default = "../mykey"
}

variable "PATH_TO_PUBLIC_KEY" {
  default = "../mykey.pub"
}

variable "TAGS" {}
