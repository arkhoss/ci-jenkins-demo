variable "AWS_REGION" {
  default = "us-east-1"
}

variable "AVAILABILITY_ZONE" {
  default = "us-east-1c"
}

variable "BLUEPRINT_ID" {
  default = "centos_7_1901_01"
}

variable "BUNDLE_ID" {
  default = "medium_2_0"
}

variable "KEYNAME" {
  default = "MASTERKDAVID"
}

variable "TAGS" {}
