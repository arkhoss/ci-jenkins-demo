#!/bin/bash
# Usage: vagrant-deploy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

cd vagrant/

pwd

mkdir -p src
cp -r ../mykey.pub src/

vagrant status

vagrant up

vagrant status

cd ..

echo "Vagrant Completed"

echo "10.0.0.101" > server_ip
