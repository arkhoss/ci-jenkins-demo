#!/bin/bash
# Usage: terraform-deploy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

SERVER_IP=$(cat server_ip)

echo "
[demo]
ci-jenkins-demo ansible_host=${SERVER_IP}
" > ansible/dev

echo "
# Ansible managed
DOCKER_OPTS=" --insecure-registry ${SERVER_IP}:8081 -dns 8.8.8.8 -dns 8.8.4.4"
" > ansible/docker/docker-envs

echo "Ansible Inventory"
cat ansible/dev

# ONLY WSL USERS
# if this fails, is because permissions are bad in the Filesystem,
# run commands below to make it work, you need to use a different drive like.. D:
# sudo umount /mnt/d
# sudo mount -t drvfs D: /mnt/d -o metadata
echo "Key permissions"
chmod 600 mykey

cd ansible

echo "Sleeping 1min"
sleep 1m

echo "Ansible - Installing Platform"
ansible-playbook ./playbooks/install-docker.yml -i dev --limit='ci-jenkins-demo' --private-key="../mykey" -u centos

echo "Ansible - Installing Artifactory and Jenkins"
ansible-playbook ./playbooks/install-artifactory-and-jenkins.yml -i dev --limit='ci-jenkins-demo' --private-key="../mykey" -u centos

echo "Ansible - Installing SonarQube"
ansible-playbook ./playbooks/install-sonarqube.yml -i dev --limit='ci-jenkins-demo' --private-key="../mykey" -u centos

cd ..

echo "Platform Installed"
