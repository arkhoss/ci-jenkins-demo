#!/bin/bash
# Usage: terraform-deploy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

echo "" > ansible/dev

echo "" > ansible/docker/docker-envs

rm -rf mykey*

rm -rf server_ip
