#!/bin/bash
# Usage: terraform-destroy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

cd terraform/

pwd

terraform plan -destroy -out=terraform.plan

terraform apply terraform.plan

cd ..

echo "Terraform Completed"
