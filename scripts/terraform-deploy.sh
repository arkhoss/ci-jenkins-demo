#!/bin/bash
# Usage: terraform-deploy
# Author: David Caballero <d@dcaballero.net>
# Version: 1.0

# safe pipefail
set -euo pipefail

cd terraform/

pwd

terraform init

terraform plan -out=terraform.plan

terraform apply terraform.plan

cd ..

echo "Terraform Completed"
