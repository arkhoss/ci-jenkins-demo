# ci-jenkins-demo

A CI demo Including Terraform, Ansible, Artifactory and Jenkins.

With this repository you will be able to deploy a full setup of tools to run Applications in an Infrastructure and Platform deployed in two different models, TAADAJA and VVADAJA. Names were created only as acronyms and not real business or professional naming, just an useful acronym.


## TAADAJA

This demo will use:

Terraform + AWS + Ansible + Docker + Artifactory + Jenkins + Angular

### [How to Install TAADAJA](https://gitlab.com/arkhoss/ci-jenkins-demo/tree/master/terraform)

![TAADAJA](/img/TAADAJA.png)



## VVADAJA

This demo will use:

Vagrant + VirtualBox + Ansible + Docker + Artifactory + Jenkins + Angular

### [How to Install VVADAJA](https://gitlab.com/arkhoss/ci-jenkins-demo/tree/master/vagrant)

![VVADAJA](/img/VVADAJA.png)
